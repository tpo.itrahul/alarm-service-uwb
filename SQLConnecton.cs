﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Print_Label
{
    class SQLConnecton
    {
        public SqlConnection con;
        public SqlConnection con1;
        public SqlConnection con2;
        public SqlDataAdapter ADP = new SqlDataAdapter();
        public SqlDataAdapter ADP1 = new SqlDataAdapter();
        public SqlDataAdapter ADP2 = new SqlDataAdapter();

        public void getConnection()
        {
            try
            {
                con = new SqlConnection();
                con.Close();
                if (con.State == ConnectionState.Open)
                {
                    con.Close();

                }
                string connectionString = ConfigurationManager.ConnectionStrings["ConStringMSSQL"].ConnectionString;
                con.ConnectionString = connectionString;
                con.Open();

            }
            catch (Exception ex)
            {
                Console.Write(ex);
                con.Close();
            }

        }



        public void getConnection1()
        {
            try
            {
                con1 = new SqlConnection();
                con1.Close();
                if (con1.State == ConnectionState.Open)
                {
                    con1.Close();

                }
                string connectionString = ConfigurationManager.ConnectionStrings["ConStringMSSQL"].ConnectionString;
                con1.ConnectionString = connectionString;
                con1.Open();

            }
            catch (Exception ex)
            {
                Console.Write(ex);
                con1.Close();
            }

        }
        public void getConnection_alert()
        {
            try
            {
                con2 = new SqlConnection();
                con2.Close();
                if (con2.State == ConnectionState.Open)
                {
                    con2.Close();

                }
                string connectionString = ConfigurationManager.ConnectionStrings["ConStringMSSQL"].ConnectionString;
                con2.ConnectionString = connectionString;
                con2.Open();

            }
            catch (Exception ex)
            {
                Console.Write(ex);
                con2.Close();
            }

        }
        public int Execute_Query(string sql_query)
        {
            int result = 0;
            getConnection1();
            try
            {
                SqlCommand cmd1 = new SqlCommand(sql_query, con1);
                result = cmd1.ExecuteNonQuery();
                con1.Close();
            }
            catch (Exception ex)
            {
                Console.Write(ex);
                con1.Close();
            }
            return result;
        }
        public int Execute_Query_alert(string sql_query)
        {
            int result = 0;
            getConnection_alert();
            try
            {
                SqlCommand cmd1 = new SqlCommand(sql_query, con2);
                result = cmd1.ExecuteNonQuery();
                con2.Close();
            }
            catch (Exception ex)
            {
                Console.Write(ex);
                con2.Close();
            }
            return result;
        }

        public DataTable DataRetrieval(string strcon)
        {
            getConnection();
            DataTable dt_Schedule = new DataTable();
            try
            {
                SqlCommand cmd = new SqlCommand(strcon, con);
                ADP.SelectCommand = cmd;
                ADP.Fill(dt_Schedule);
                con.Close();
            }
            catch (Exception ex)
            {
                Console.Write(ex);
                con.Close();
            }
            return dt_Schedule;
        }

        public DataTable Fetch_Alerts()
        {
            DataTable dt_Data = new DataTable();
            string ls_sql = "Select * From UWB_Alerts where  Status = 0 ";
            dt_Data = DataRetrieval(ls_sql);
            return dt_Data;
        }
        public DataTable Fetch_treadentry(string mhe_Name)
        {
            DataTable dt_Data = new DataTable();
            string ls_sql = "Select LEAF_TRUCKNO From [FIFO].[dbo].[DEXT_TREADENTRY] where LEAF_TRUCKNO ='"+ mhe_Name + "' and STATUS = 'FALSE' and SCRAP ='FALSE' and HOLD ='FALSE' ";
            dt_Data = DataRetrieval(ls_sql);
            return dt_Data;
        }

        public int Update_Alerts(string tag_Name)
        {

            string ls_sql = "update  UWB_Alerts set Upadated_Date_Time ='" + DateTime.Now.ToString("yyyy-MM-dd H:mm") + "', Status = 1 where  Tag_Name ='" + tag_Name + "'";
            return Execute_Query(ls_sql);
        }

        public DataTable Fetch_tagname(string Mhe_Id)

        {

            DataTable dt_Data = new DataTable();

            string ls_sql = "Select * From UWB_Tag_Mapping where UWB_Tag_Id='" + Mhe_Id + "' and status=1 ";

            dt_Data = DataRetrieval(ls_sql);

            return dt_Data;

        }

        public int updatetreadentry(string mhe_Name, string Extruder_Tag_No)

        {
            string ls_sql = "update  [FIFO].[dbo].[DEXT_TREADENTRY] set STATUS ='TRUE',DateTimeUpdated_ByAlerts='" + DateTime.Now.ToString("yyyy-MM-dd H:mm") + "'  where LEAF_TRUCKNO = '" + mhe_Name + "' and STATUS ='FALSE' and PKDEXTID ='" + Extruder_Tag_No + "'";

            return Execute_Query_alert(ls_sql);

        }
            
        public int Update_LeafTruckmaster(string Mhe_Name)

        {
            string ls_sql = "update  [FIFO].[dbo].[Leaf_Truck_Master] set LeafTruck_Status ='T'  where LeafTruck_No = '" + Mhe_Name + "'";

            return Execute_Query_alert(ls_sql);

        }

        public int UpdateAlarm(string Zone_Id, string Mhe_Id)
        {
            int a = 1;
            bool status = Convert.ToBoolean(a);

            string ls_sql = "update  UWB_Alarms set  status ='" + status + "',Tag_Name ='" + Mhe_Id + "',Date_Time ='" + DateTime.Now.ToString() + "' where Zone_Id ='" + Zone_Id + "'";
            return Execute_Query_alert(ls_sql);
        }
        public int updatealerts(string Mhe_Id)

        {



            string ls_sql = "update UWB_Alerts set Upadated_Date_Time ='" + DateTime.Now.ToString("yyyy-MM-dd H:mm") + "',status = 1 where Tag_Name = '" + Mhe_Id + "' and status = 0 ";

            return Execute_Query(ls_sql);

        }
    }

}

