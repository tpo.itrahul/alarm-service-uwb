﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Timers;
using System.Configuration;
using System.Data.SqlClient;
using System.Data;
using System.Drawing;
using System.Drawing.Printing;

namespace Print_Label
{
    public class Printservice
    {
        private readonly Timer _timer;

        static DataTable Print_Label = new DataTable();
        Connections sh = new Connections();

        SQLConnecton sg = new SQLConnecton();

        public string Tag_Name { get; set; }
        public string zone_Id { get; set; }
        public Printservice()
        {

            _timer = new Timer(60000) { AutoReset = true };
            _timer.Elapsed += timerElapsed;

        }

        private void timerElapsed(object sender, ElapsedEventArgs e)
        {
            //Console.WriteLine("UWB Alarm Service Started");
            string[] lines = new string[] { "UWB Alarm Service Started : " + DateTime.Now.ToString() };
            File.AppendAllLines(@"C:\Demo\UwbAlarmservice.txt", lines);
            GetProduction_DE();
            UpdateEmptyLeaftruck();

        }



        public void GetProduction_DE()
        {
            try
            {
                DataTable Production = sg.Fetch_Alerts();

                foreach (DataRow row in Production.Rows)
                {
                    if (row != null) // This will check the null values also (if you want to check).
                    {
                        var Mhe_Id = row[1].ToString();
                        var Zone_Id = row[2].ToString();
                        //Console.WriteLine("Tag_Name  from UWB Alerts:  " + Mhe_Id + " : " + Zone_Id);
                        //Console.WriteLine("");
                        //Console.WriteLine("");
                        var Extruder_Tag_No = row[8].ToString();
                        sg.UpdateAlarm(Zone_Id, Mhe_Id);  //insert data into alarm table
                        var currentTime = DateTime.Now;
                        var entryTime = Convert.ToDateTime(row[4].ToString());
                        TimeSpan diffdateTime = currentTime - entryTime;
                        if (diffdateTime.TotalSeconds > 180)                 //checking if leaf truck out side the zone for more than 3 min
                        {
                            DataTable TRUCKNO = sg.Fetch_tagname(Mhe_Id);    //Fetching corresponding leaf truck number against Tag Id from RTLS
                            DataRow r = TRUCKNO.Rows[0];
                            var mhe_Name = r[1].ToString();
                            //Console.WriteLine("Updated tread entry Leaf Truck No: " + mhe_Name);
                            //Console.WriteLine("");
                            //Console.WriteLine("");
                            sg.updatetreadentry(mhe_Name, Extruder_Tag_No);  //updating production table to remove leaf truck which are out side the zone for more than 3 min . Remove data from stock
                                                                             // sg.Update_LeafTruckmaster(mhe_Name); //update leaftruck master 
                                                                             //Console.WriteLine("Updated Alert Tag Id: " + Mhe_Id);
                                                                             //Console.WriteLine("");
                                                                             //Console.WriteLine("");
                            sg.updatealerts(Mhe_Id);                       //updating alert table status to 1 and inserting  updated_time to current time
                        }
                        else
                        {
                            string[] lines = new string[] { "Tag Name :  " + Mhe_Id + "update in to UWD Alerts" + " on: " + DateTime.Now.ToString() };
                            File.AppendAllLines(@"C:\Demo\UwbAlarmservice.txt", lines);
                            GetMainZones(Mhe_Id);

                        }
                    }
                    else
                    {
                        //Console.WriteLine("No  Alerts");
                        string[] lines = new string[] { "No  Alerts" + " on: " + DateTime.Now.ToString() };
                        File.AppendAllLines(@"C:\Demo\UwbAlarmservice.txt", lines);
                    }
                }
            }
            catch (Exception ex)
            {
                //Console.WriteLine(ex);
                string[] lines = new string[] { ex + " on: " + DateTime.Now.ToString() };
                File.AppendAllLines(@"C:\Demo\UwbAlarmservice.txt", lines);
            }
        }
        private void GetMainZones(string Mhe_Id)
        {
            DataTable Mainzones = sh.Fetch_MainZones(Mhe_Id);
            if (Mainzones.Rows.Count > 0)
            {
                DataRow row = Mainzones.Rows[0];
                var tag_Name = row[1].ToString();
                //Console.WriteLine("Leaf Truck Returned   " + tag_Name);
                //Console.WriteLine("");
                //Console.WriteLine("");
                string[] lines = new string[] { DateTime.Now.ToString() + "   Leaf Truck Returned   " + tag_Name };
                File.AppendAllLines(@"C:\Demo\UwbAlarmservice.txt", lines);
                sg.Update_Alerts(tag_Name);
                sh.UpdateMainZones(tag_Name);

            }
            else
            {
                //Console.WriteLine("Leaf Truck Not Returned");
                string[] lines = new string[] { DateTime.Now.ToString() + "  Leaf Truck Not Returned" };
                File.AppendAllLines(@"C:\Demo\UwbAlarmservice.txt", lines);
            }
        }
        public void UpdateEmptyLeaftruck()
        {
            DataTable Reserved_zones = sh.Fetch_Reserved_zones();
            if (Reserved_zones.Rows.Count > 0)
            {
                foreach (DataRow row in Reserved_zones.Rows)
                {
                    var Mhe_Id = row[0].ToString();
                    //Console.WriteLine("Leaf Truck Tag Name from Reserved positions :  " + Mhe_Id);
                    DataTable TRUCKNO = sg.Fetch_tagname(Mhe_Id);   //Fetching corresponding leaf truck number against Tag Id from RTLS
                    if (TRUCKNO.Rows.Count > 0)
                    {
                        DataRow truck = TRUCKNO.Rows[0];
                        var mhe_Name = truck[1].ToString();

                        //Console.WriteLine("Leaf Truck no from uwb Tag mapping :  " + mhe_Name);
                        //Console.WriteLine("");
                        //Console.WriteLine("");
                        DataTable treadentry = sg.Fetch_treadentry(mhe_Name);
                        if (treadentry.Rows.Count > 0)
                        {
                            /// Console.WriteLine("in stock");
                            /// Console.WriteLine("");
                            /// Console.WriteLine("");
                            // DataRow emptylftrk = treadentry.Rows[0];
                            //var Mhe_Name = emptylftrk[0].ToString();
                        }
                        else
                        {

                            // Console.WriteLine("Leaf Truck name not in treadentry, Empty Leaf Truck :  " + mhe_Name);
                            //  Console.WriteLine("");
                            // Console.WriteLine("");
                            sg.Update_LeafTruckmaster(mhe_Name);
                            //  Console.WriteLine("Updated Leaftruck master");
                            // Console.WriteLine("");
                            // Console.WriteLine("");
                        }
                    }
                    else
                    {
                        //Console.WriteLine("No data found in uwb tag mapping");

                    }
                }


            }
            else
            {
                // Console.WriteLine("No data found in Reserved position");
            }
        }



        public void Start()
        {
            _timer.Start();
        }

        public void Stop()
        {
            _timer.Stop();
        }
    }
}
