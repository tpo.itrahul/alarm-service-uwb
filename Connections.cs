﻿using MySqlConnector;
using System;
using System.Configuration;
using System.Data;

namespace Print_Label
{
    class Connections
    {
        public MySqlConnection con;
        public MySqlConnection con1;
        public MySqlDataAdapter ADP = new MySqlDataAdapter();
        public MySqlDataAdapter ADP1 = new MySqlDataAdapter();

        public void GetConnection()
        {
            try
            {
                con = new MySqlConnection();
                con.Close();
                if (con.State == ConnectionState.Open)
                {
                    con.Close();

                }
                string connectionString = ConfigurationManager.ConnectionStrings["ConStringMySQL"].ConnectionString;
                con.ConnectionString = connectionString;
                con.Open();

            }
            catch (Exception ex)
            {
                Console.Write(ex);
                con.Close();
            }
        }

        public void GetConnection1()
        {
            try
            {
                con1 = new MySqlConnection();
                con1.Close();
                if (con1.State == ConnectionState.Open)
                {
                    con1.Close();

                }
                string connectionString = ConfigurationManager.ConnectionStrings["ConStringMySQL"].ConnectionString;
                con1.ConnectionString = connectionString;
                con1.Open();

            }
            catch (Exception ex)
            {
                Console.Write(ex);
                con1.Close();
            }
        }



        public int Execute_Query(string sql_query)
        {
            int result = 0;
            GetConnection();
            try
            {
                MySqlCommand cmd = new MySqlCommand(sql_query, con);
                result = cmd.ExecuteNonQuery();
                con.Close();
            }
            catch (Exception ex)
            {
                Console.Write(ex);
                con.Close();
            }
            return result;
        }
        public DataTable DataRetrieval(string strcon)
        {
            GetConnection1();
            DataTable dt_Data = new DataTable();
            try
            {
                MySqlCommand cmd1 = new MySqlCommand(strcon, con1);
                ADP1.SelectCommand = cmd1;
                ADP1.Fill(dt_Data);
                con1.Close();
            }
            catch (Exception ex)
            {
                Console.Write(ex);
                con1.Close();
            }
            return dt_Data;
        }

        public DataTable Fetch_MainZones(string Mhe_Id)
        {
            DataTable dt_Data = new DataTable();
           // string print_sc = "Select * from UWB_Main_Zone where Tag_Name ='"+ Mhe_Id +"' and Status = 0 and  Action  ";
            string print_sc = "Select * from UWB_Main_Zoneupdates where Fetching_Status= 0 and  Tag_Name ='"+ Mhe_Id +"'  and action='in' and Zone_id in ('171','146') and Tag_Name not in (Select Tag_Name from UWB_Reserved_Position where Zone_Id is not null and Zone_Id in ('173','145','157','158','167'))";
           // string print_sc = "Select * from UWB_Main_Zoneupdates where Fetching_Status= 0 and  Tag_Name ='"+ Mhe_Id +"'  and action='in' and Zone_id in ('171','146') and Tag_Name not in (Select Tag_Name from UWB_Reserved_Position where Zone_Id is not null and Zone_Id in ('173','145','157','158','167'))";
            dt_Data = DataRetrieval(print_sc);
            return dt_Data;
        }
        public DataTable Fetch_Reserved_zones()
        {
            DataTable dt_Data = new DataTable();
           // string print_sc = "Select * from UWB_Main_Zone where Tag_Name ='"+ Mhe_Id +"' and Status = 0 and  Action  ";
            string print_sc = "Select Tag_Name from UWB_Reserved_Position where Zone_Id is not null";
           // string print_sc = "Select * from UWB_Main_Zoneupdates where Fetching_Status= 0 and  Tag_Name ='"+ Mhe_Id +"'  and action='in' and Zone_id in ('171','146') and Tag_Name not in (Select Tag_Name from UWB_Reserved_Position where Zone_Id is not null and Zone_Id in ('173','145','157','158','167'))";
            dt_Data = DataRetrieval(print_sc);
            return dt_Data;
        }
        public int UpdateMainZones(string tag_Name)
        {
            string ls_sql = "update UWB_Main_Zoneupdates set Fetching_Status = 1 where Tag_Name ='" + tag_Name + "' and Action = 'in'";
            return Execute_Query(ls_sql);
        }

    }
}
