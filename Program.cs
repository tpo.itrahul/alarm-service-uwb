﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Topshelf;

namespace Print_Label
{
    class Program
    {
        static void Main(string[] args)
        {
            var exitCode = HostFactory.Run(x =>
            {
                x.Service<Printservice>(s =>
                {
                    s.ConstructUsing(printservice => new Printservice());
                    s.WhenStarted(printservice => printservice.Start());
                    s.WhenStopped(printservice => printservice.Stop());
                });
                x.RunAsLocalSystem();
                x.SetServiceName("UWB AlarmService");
                x.SetDisplayName("UWB  Alarm Service");
                x.SetDescription("UWB  Alarm Service is used in Traceme application for  Alarm.");
            });
            int exitCodeValue = (int)Convert.ChangeType(exitCode, exitCode.GetTypeCode());
            Environment.ExitCode = exitCodeValue;
        }
    }
}
